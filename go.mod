module gitee.com/lucky51/go-xlsx-templater

go 1.13

require (
	gitee.com/lucky51/xlsx v1.0.7
	github.com/aymerick/raymond v2.0.2+incompatible
	github.com/ivahaev/go-xlsx-templater v0.0.0-20200217104802-1394ee35aab8
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
